package com.M17.rudy17test.api.data.entity

/**
 * Created by Rudy on 2019/7/21
 */
class UserInfoEntity {
    var id: Long? = null
    var name: String? = null
    var avatarUrl: String? = null
    var score: Float = 0.toFloat()
}
