package com.M17.rudy17test.api.data.vo

import com.M17.rudy17test.api.data.entity.UserInfoEntity

import java.io.Serializable

/**
 * Created by Rudy on 2019/7/21
 */
class UserSearchInfoVo : Serializable {

    /**
     * login : mispanda
     * id : 19166342
     * node_id : MDQ6VXNlcjE5MTY2MzQy
     * avatar_url : https://avatars2.githubusercontent.com/u/19166342?v=4
     * gravatar_id :
     * url : https://api.github.com/users/mispanda
     * html_url : https://github.com/mispanda
     * followers_url : https://api.github.com/users/mispanda/followers
     * following_url : https://api.github.com/users/mispanda/following{/other_user}
     * gists_url : https://api.github.com/users/mispanda/gists{/gist_id}
     * starred_url : https://api.github.com/users/mispanda/starred{/owner}{/repo}
     * subscriptions_url : https://api.github.com/users/mispanda/subscriptions
     * organizations_url : https://api.github.com/users/mispanda/orgs
     * repos_url : https://api.github.com/users/mispanda/repos
     * events_url : https://api.github.com/users/mispanda/events{/privacy}
     * received_events_url : https://api.github.com/users/mispanda/received_events
     * type : User
     * site_admin : false
     * score : 34.582653
     */

    var login: String? = null
    var id: Long = 0
    var node_id: String? = null
    var avatar_url: String? = null
    var gravatar_id: String? = null
    var url: String? = null
    var html_url: String? = null
    var followers_url: String? = null
    var following_url: String? = null
    var gists_url: String? = null
    var starred_url: String? = null
    var subscriptions_url: String? = null
    var organizations_url: String? = null
    var repos_url: String? = null
    var events_url: String? = null
    var received_events_url: String? = null
    var type: String? = null
    var isSite_admin: Boolean = false
    var score: Double = 0.toDouble()

    fun toEntity(): UserInfoEntity {
        val userInfoEntity = UserInfoEntity()
        userInfoEntity.id = this.id
        userInfoEntity.name = this.login
        userInfoEntity.avatarUrl = this.avatar_url
        userInfoEntity.score = score.toFloat()
        return userInfoEntity
    }
}