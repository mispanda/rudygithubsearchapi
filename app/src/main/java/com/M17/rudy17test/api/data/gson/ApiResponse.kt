package com.M17.rudy17test.api.data.gson

import java.io.Serializable

/**
 * Created by Rudy on 2019/7/21
 */
class ApiResponse<T> : Serializable {
    var code = 100
    var total_count: Int = 0
    var incomplete_results: Boolean = false
    var items: T? = null
    var message: String? = null

    /**
     * @return True: Response Success (code == 100)
     */
    val isSuccess: Boolean
        get() = code == 100

    companion object {

        fun standardize(string: String?): String {
            return string?.toLowerCase() ?: ""
        }
    }
}
