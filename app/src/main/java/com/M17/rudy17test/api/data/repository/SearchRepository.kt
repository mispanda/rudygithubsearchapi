package com.M17.rudy17test.api.data.repository

import com.M17.rudy17test.api.data.gson.ApiResponse
import com.M17.rudy17test.api.data.vo.UserSearchInfoVo
import com.M17.rudy17test.util.network.HttpManager

import io.reactivex.Observable

/**
 * Created by Rudy on 2019/7/21
 */
class SearchRepository private constructor(
        /**
         * Api Source
         */
        private val mHttpManager: HttpManager) {

    // region API

    /**
     * 取得使用者資料 Observable -
     * @return Observable<ApiResponse></ApiResponse><ArrayList></ArrayList><UserSearchInfoVo>>>
    </UserSearchInfoVo> */
    fun getMyUserProfileNew(userName: String, page: Int): Observable<ApiResponse<List<UserSearchInfoVo>>> {
        return mHttpManager.getUserSearchObservable(userName, page)
    }

    companion object {

        /**
         * for singleton creation
         */
        private val LOCK = Any()

        /**
         * Singleton instance
         */
        private var sInstance: SearchRepository? = null;

        /**
         * get PostRepository Singleton instance
         * @return Singleton instance
         */
        fun getInstance(httpManager: HttpManager): SearchRepository? {
            if (sInstance == null) {
                synchronized(LOCK) {
                    if (sInstance == null) {
                        sInstance = SearchRepository(httpManager)
                    }
                }
            }
            return sInstance
        }
    }
    // endregion

}
