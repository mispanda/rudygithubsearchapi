package com.M17.rudy17test.api.data.vo

import java.io.Serializable

/**
 * Created by Rudy on 2019/7/22
 */
class BaseVo<T> : Serializable {

    var total_count: Int = 0
    var isIncomplete_results: Boolean = false
    var items: T? = null
    var message: String? = null
}
