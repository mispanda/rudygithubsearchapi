package com.M17.rudy17test.activity.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.M17.rudy17test.activity.view.model.MainSearchViewMode
import com.M17.rudy17test.api.data.repository.SearchRepository

/**
 * Search ViewModel Factory
 * ViewModel 無法被 new 建構，要於建構時傳入參數需透過 Factory)
 * Created by Rudy on 2019/7/21
 */
class SearchViewModelFactory(private val mSearchRepository: SearchRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainSearchViewMode(mSearchRepository) as T
    }
}
