package com.M17.rudy17test.activity.view.model

import android.util.Log

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.M17.rudy17test.api.data.entity.UserInfoEntity
import com.M17.rudy17test.api.data.repository.SearchRepository
import com.M17.rudy17test.api.data.vo.UserSearchInfoVo
import com.M17.rudy17test.util.EventBusUtil
import com.M17.rudy17test.util.network.BaseObserver

import java.util.ArrayList

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Rudy on 2019/7/21
 */
class MainSearchViewMode(private val mSearchRepository: SearchRepository) : ViewModel() {

    private val TAG = MainSearchViewMode::class.java.simpleName

    /**
     * Observables 行程管理
     */
    private val mCompositeDisposable: CompositeDisposable

    /**
     * 目前載入頁數
     */
    private var mPage = 1
    private var mTotal = 0

    val userInfo: MutableLiveData<List<UserInfoEntity>>


    init {
        mCompositeDisposable = CompositeDisposable()
        userInfo = MutableLiveData()
    }

    fun fetchSearchUser(userName: String) {

        val userInfoEntities = userInfo.value
        if (mTotal == 0 || userInfoEntities != null && userInfoEntities.size < mTotal) {
            fetchSearchUser(userName, mPage)
        }
    }

    fun fetchSearchUser(userName: String, page: Int) {
        Log.e(TAG, "user name :$userName   page:$page")
        mSearchRepository.getMyUserProfileNew(userName, page).subscribe(object : BaseObserver<List<UserSearchInfoVo>>() {
            override fun onSubscribe(d: Disposable) {
                mCompositeDisposable.add(d)
            }

            override fun onSuccess(t: List<UserSearchInfoVo>?, total: Int) {
                if (t != null) {
                    mPage++
                    mTotal = total
                    val userInfoAPIEntityList = ArrayList<UserInfoEntity>()
                    for (userSearchInfoVo in t) {
                        val contentEntity = userSearchInfoVo.toEntity()
                        userInfoAPIEntityList.add(contentEntity)
                    }


                    if (userInfo.value == null) {
                        userInfo.setValue(userInfoAPIEntityList)
                    } else {
                        var userInfoLocalEntities = ArrayList<UserInfoEntity>()
                        userInfoLocalEntities.addAll(userInfo.value!!)
                        userInfoLocalEntities.addAll(userInfoAPIEntityList)
                        userInfo.setValue(userInfoLocalEntities)
                    }

                    val userSearchInfoEvent = EventBusUtil.OnUserSearchInfoEvent<UserInfoEntity>()
                    userSearchInfoEvent.setSuccess(true)
                    EventBusUtil.postEvent(userSearchInfoEvent)
                }
            }

            override fun onFailure(code: Int, message: String?) {
                super.onFailure(code, message)
                val userSearchInfoEvent = EventBusUtil.OnUserSearchInfoEvent<UserInfoEntity>()
                userSearchInfoEvent.setSuccess(false)
                EventBusUtil.postEvent(userSearchInfoEvent)
            }
        })
    }

    /**
     * 清除 並 關閉 所有此VM管理的 Observable
     */
    fun disposeAll() {
        mCompositeDisposable.clear()
    }

    /**
     * 清除所有 觀察 VM's ViewObject LiveData 的 Observer
     * @param lifecycleOwner 生命週期擁有者，與此VM相關的View
     */
    fun unregisterLiveDataVO(lifecycleOwner: LifecycleOwner) {
        this.userInfo.removeObservers(lifecycleOwner)
    }

    /**
     * 清空 list
     */
    fun clearListAndTimestamp() {
        userInfo.value = null
        mPage = 1
        mTotal = 0
    }

}
