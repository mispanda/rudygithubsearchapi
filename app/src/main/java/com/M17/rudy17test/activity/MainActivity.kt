package com.M17.rudy17test.activity

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import com.M17.rudy17test.R
import com.M17.rudy17test.activity.view.model.MainSearchViewMode
import com.M17.rudy17test.adapter.MainSearchAdapter
import com.M17.rudy17test.api.data.entity.UserInfoEntity
import com.M17.rudy17test.util.EventBusUtil
import com.M17.rudy17test.util.InjectorUtils
import com.M17.rudy17test.util.RecyclerViewScrollListener
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity() {

    private val mEdSearchName : EditText by bindView(R.id.ed_main_search_name)
    private val mBtnSearch : Button by bindView(R.id.btn_main_search)
    private val mSRLSearch : SwipeRefreshLayout by bindView(R.id.swipe_main_layout)
    private val mRvUserInfo : RecyclerView by bindView(R.id.ry_main_user_search_info)

    private fun <T : View> Activity.bindView(@IdRes res: Int): Lazy<T> {
        return lazy { findViewById<T>(res) }
    }

    private var mMainSearchViewMode: MainSearchViewMode? = null
    private var mAdapter: MainSearchAdapter? = null
    private var mSearchWord: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        val viewModelFactory = InjectorUtils.provideSearchRepositoryVMFactory()
        mMainSearchViewMode = ViewModelProviders.of(this, viewModelFactory)
                .get(MainSearchViewMode::class.java)

        mMainSearchViewMode!!.userInfo
                .observe(this, Observer<List<UserInfoEntity>> { this.updateUserInfo(it) })

        initSwipeRefreshView()
        initRecycleView()
        mBtnSearch.setOnClickListener { commentBtn()}
    }

    override fun onResume() {
        super.onResume()
        EventBusUtil.register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBusUtil.unregister(this)
    }

    override fun onDestroy() {
        mMainSearchViewMode!!.disposeAll()
        // unregister LiveData
        mMainSearchViewMode!!.unregisterLiveDataVO(this)

        super.onDestroy()
    }

    /**
     * setUp SwipeRefreshLayout for recyclerView list refresh mechanism
     */
    private fun initSwipeRefreshView() {
        // 更新旋轉圖示設定：旋轉icon為 主色
        mSRLSearch.setColorSchemeResources(R.color.colorPrimary)
        // 刷新 畫面
        mSRLSearch.setOnRefreshListener({ this.refreshView() })
    }

    private fun initRecycleView() {
        mAdapter = MainSearchAdapter(this)
        mRvUserInfo.layoutManager = LinearLayoutManager(this)
        mRvUserInfo.adapter = mAdapter

        val loadMoreListener = RecyclerViewScrollListener(
                RecyclerViewScrollListener.LAYOUT_MANAGER_TYPE.LINEAR)

        loadMoreListener.setOnLoadMoreListener { mMainSearchViewMode!!.fetchSearchUser(mSearchWord) }
        mRvUserInfo.addOnScrollListener(loadMoreListener)
    }

    /**
     * 刷新整個畫面
     */
    private fun refreshView() {
        commentBtn()
    }

    private fun updateUserInfo(userInfoEntityList: List<UserInfoEntity>?) {
        mSRLSearch.isRefreshing = false
        if (mAdapter != null && userInfoEntityList != null) {
            mAdapter!!.setUserInfoList(userInfoEntityList)
        }
    }

    /**
     * 點選下方Bar 留言圖示按鈕 Scroll to first comment.
     */
    private fun commentBtn() {
        mEdSearchName.clearFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(mEdSearchName.windowToken, 0)

        val searchName = mEdSearchName.text.toString()
        if (TextUtils.isEmpty(searchName)) {
            Toast.makeText(this, R.string.alert_edit_search, Toast.LENGTH_SHORT).show()
        } else {
            mMainSearchViewMode!!.clearListAndTimestamp()
            mSearchWord = searchName
            mMainSearchViewMode!!.fetchSearchUser(searchName)
        }
    }

    /**
     * Post GitHub Event
     *
     * @param event onUserSearchInfo Complete
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUserSearchInfoEvent(event: EventBusUtil.OnUserSearchInfoEvent<*>) {
        if (event.isSuccess()) {

        } else {
            Toast.makeText(this, R.string.alert_loading_error, Toast.LENGTH_LONG).show()
        }
    }
}
