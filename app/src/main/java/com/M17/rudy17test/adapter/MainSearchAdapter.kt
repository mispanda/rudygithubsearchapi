package com.M17.rudy17test.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.M17.rudy17test.R
import com.M17.rudy17test.adapter.ViewHolder.RecyclerViewBaseHolder
import com.M17.rudy17test.api.data.entity.UserInfoEntity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.util.*


/**
 * Created by Rudy on 2019/7/21
 */
class MainSearchAdapter(private val mContext: Context) : RecyclerView.Adapter<MainSearchAdapter.UserInfoViewHolder>() {

    private var mUserInfoEntities: List<UserInfoEntity>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserInfoViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user, parent, false)
        return UserInfoViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: UserInfoViewHolder, position: Int) {
        val userInfoEntity = mUserInfoEntities!![position]
        val options = RequestOptions()
                .fitCenter()
                .circleCrop()

        Glide.with(mContext)
                .load(userInfoEntity.avatarUrl)
                .apply(options)
                .into(viewHolder.imgAvatar)

        if (!TextUtils.isEmpty(userInfoEntity.name)) {
            viewHolder.tvUserName.visibility = View.VISIBLE
            viewHolder.tvUserName.text = mContext.getString(R.string.user_item_name, userInfoEntity.name)
        } else {
            viewHolder.tvUserName.visibility = View.GONE
        }

        if (userInfoEntity.score > 0) {
            viewHolder.tvUserScore.visibility = View.VISIBLE
            viewHolder.tvUserScore.text = mContext.getString(R.string.user_item_score, userInfoEntity.score)
        } else {
            viewHolder.tvUserScore.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return if (mUserInfoEntities != null) {
            mUserInfoEntities!!.size
        } else 0
    }

    fun setUserInfoList(userInfoList: List<UserInfoEntity>) {
        //        this.mTotal = total;
        this.mUserInfoEntities = userInfoList
        notifyDataSetChanged()
    }

    inner class UserInfoViewHolder internal constructor(itemView: View) : RecyclerViewBaseHolder(itemView) {
        val imgAvatar: ImageView = itemView.findViewById(R.id.img_item_user_avatar)
        val tvUserName = itemView.findViewById<TextView>(R.id.tv_item_user_name)
        val tvUserScore = itemView.findViewById<TextView>(R.id.tv_item_user_score)
    }

    inner class UserNoDataViewHolder internal constructor(itemView: View) : RecyclerViewBaseHolder(itemView)
}
