package com.M17.rudy17test.adapter.ViewHolder


import android.view.View
import butterknife.ButterKnife
import androidx.recyclerview.widget.RecyclerView.*

/**
 * Created by Rudy on 2019/7/21
 * To use the butter knife, please extend this class
 */
open class RecyclerViewBaseHolder(itemView: View) : ViewHolder(itemView) {
    init {
        ButterKnife.bind(this, itemView)
    }
}