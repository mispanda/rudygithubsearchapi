package com.M17.rudy17test

import android.app.Application
import android.content.Context

/**
 * Created by Rudy on 2019/7/21
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        mContext = this
    }

    companion object {
        private var mContext: Context? = null
    }
}
