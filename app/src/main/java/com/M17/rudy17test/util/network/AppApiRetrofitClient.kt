package com.M17.rudy17test.util.network

import com.M17.rudy17test.util.Constant
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * Created by Rudy on 2019/7/21
 */
class AppApiRetrofitClient internal constructor() {

    val apiService: AppApiService

    init {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
        httpClient.readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
        httpClient.writeTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
        httpClient.addInterceptor(NetworkInterceptor())
        httpClient.addInterceptor(HeaderInterceptor_v2())
        // add logging as last interceptor
        httpClient.addInterceptor(logging)  // <-- this is the important line!

        val retrofit = Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()

        apiService = retrofit.create(AppApiService::class.java)
    }

    inner class NetworkInterceptor : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            //      if (!MyApplication.getIsNetworkConnected())
            //        throw new NoNetworkException();
            //      else
            return chain.proceed(chain.request())
        }
    }

    inner class NoNetworkException : IOException()

    class HeaderInterceptor_v2 : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder() // <-- this is the important line
            requestBuilder.header("Content-Type", "application/json")
            requestBuilder.header("Accept", "application/json")

            requestBuilder.method(original.method(), original.body())
            val request = requestBuilder.build()
            return chain.proceed(request)
        }
    }

    companion object {

        val instance = AppApiRetrofitClient()
        private val TIMEOUT = 60
    }
}
