package com.M17.rudy17test.util

import android.content.Context
import android.widget.Toast

/**
 * Created by Rudy on 2019/7/21
 */
object Constant {
    val API_URL = "https://api.github.com/"

    val PAGE_COUNT = 20

    fun toastAlert(context: Context, msg: String?) {
        if (msg != null && msg.length > 0) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }
    }
}
