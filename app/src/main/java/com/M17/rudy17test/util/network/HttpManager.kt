package com.M17.rudy17test.util.network

import android.util.Log

import com.M17.rudy17test.api.data.gson.ApiResponse
import com.M17.rudy17test.api.data.vo.UserSearchInfoVo
import com.M17.rudy17test.util.Constant

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Rudy on 2019/7/21
 */
class HttpManager {
    val TAG = HttpManager::class.java.simpleName

    private val apiService: AppApiService
    private val mCompositeDisposable: CompositeDisposable

    /**
     * Constructor
     */
    init {
        apiService = AppApiRetrofitClient.instance.apiService
        mCompositeDisposable = CompositeDisposable()
    }

    fun addToCompositeDisposable(disposable: Disposable) {
        mCompositeDisposable.add(disposable)
    }

    fun disposeAll() {
        mCompositeDisposable.clear()
        Log.d(TAG, "HttpManager disposeAll!")
    }

    /**
     *
     * @return Observable 取的搜尋使用者
     */
    fun getUserSearchObservable(userName: String, page: Int): Observable<ApiResponse<List<UserSearchInfoVo>>> {
        val observable = apiService.getSearchUserInfo(userName, page, Constant.PAGE_COUNT)
        return observable.compose(RxObservableUtils.applySchedulers())
    }
}
