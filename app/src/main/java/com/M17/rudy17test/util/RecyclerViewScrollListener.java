package com.M17.rudy17test.util;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 * Created by Rudy on 2019/7/22
 */
public class RecyclerViewScrollListener extends RecyclerView.OnScrollListener{

    private String TAG = getClass().getSimpleName();

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public enum LAYOUT_MANAGER_TYPE {
        LINEAR,
        GRID,
        STAGGERED_GRID
    }

    /**
     * layoutManager的類型
     */
    protected LAYOUT_MANAGER_TYPE layoutManagerType;

    /**
     * 最後一個位置
     */
    private int[] lastPositions;

    /**
     * 最後一個可以見的item位置
     */
    private int lastVisibleItemPosition;

    /**
     * 目前的滑動狀態
     */
    private int currentScrollState = 0;

    private int triggerPosition = 1;

    boolean isLoadMoreEnabled;

    private OnLoadMoreListener mOnLoadMoreListener;

    private Boolean startLoading = false;

    /**
     * Constructor
     *
     * @param type recycler view type
     */
    public RecyclerViewScrollListener(LAYOUT_MANAGER_TYPE type) {
        this.layoutManagerType = type;
        isLoadMoreEnabled = true;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        //  int lastVisibleItemPosition = -1;
        if (layoutManagerType == null) {
            if (layoutManager instanceof LinearLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.LINEAR;
            } else if (layoutManager instanceof GridLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.GRID;
            } else if (layoutManager instanceof StaggeredGridLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.STAGGERED_GRID;
            } else {
                throw new RuntimeException(
                        "Unsupported LayoutManager used. Valid ones are LinearLayoutManager, GridLayoutManager and StaggeredGridLayoutManager");
            }
        }

        switch (layoutManagerType) {
            case LINEAR:
                lastVisibleItemPosition = ((LinearLayoutManager) layoutManager)
                        .findLastVisibleItemPosition();
                break;
            case GRID:
                lastVisibleItemPosition = ((GridLayoutManager) layoutManager)
                        .findLastVisibleItemPosition();
                break;
            case STAGGERED_GRID:
                StaggeredGridLayoutManager staggeredGridLayoutManager
                        = (StaggeredGridLayoutManager) layoutManager;
                if (lastPositions == null) {
                    lastPositions = new int[staggeredGridLayoutManager.getSpanCount()];
                }
                staggeredGridLayoutManager.findLastVisibleItemPositions(lastPositions);
                lastVisibleItemPosition = findMax(lastPositions);
                break;
            default:
                break;
        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        currentScrollState = newState;
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();

        //    Log.d("RecyclerView", "visibleItemCount:"+visibleItemCount+" lastVisibleItemPosition:"+lastVisibleItemPosition+" totalItemCount:"+totalItemCount+" triggerPosition:"+triggerPosition+" end:"+(totalItemCount - triggerPosition - (Constant.DEFAILT_PERPAGE_COUNT/2)));

        // if not pre load
    /*if (isLoadMoreEnabled &&
        (visibleItemCount > 0
            && (lastVisibleItemPosition)
            >= (totalItemCount - triggerPosition))) {*/

        // if want pre load
        if (isLoadMoreEnabled &&
                (visibleItemCount > 0
                        && (lastVisibleItemPosition)
                        >= (totalItemCount - triggerPosition - ( Constant.INSTANCE.getPAGE_COUNT() / 2)))) { //Position to start load more

            if(!startLoading) {
                // Log.d("RecyclerView", "load more");
                startLoading = true;
                if (mOnLoadMoreListener != null) {
                    mOnLoadMoreListener.onLoadMore();
                }
            } else if(startLoading && currentScrollState == RecyclerView.SCROLL_STATE_IDLE) {
                startLoading = false;
            }
        }
    }

    public void loadComplete()
    {
        startLoading = false;
    }

    private int findMax(int[] lastPositions) {
        int max = lastPositions[0];
        for (int value : lastPositions) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }

    /**
     * Enable / disable loadMore callback
     *
     * @param enable true to enable the call back
     */
    public void enableLoadMore(boolean enable) {
        this.isLoadMoreEnabled = enable;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.mOnLoadMoreListener = onLoadMoreListener;
    }

    /**
     * Start load more when the view of the position show E.g. A List with 10 item, if set to 3, will
     * start to load more when the 10 - 3 = 7th item is show on screen
     *
     * @param triggerPosition Count from bottom to top
     */
    public void setTriggerPosition(int triggerPosition) {
        this.triggerPosition = triggerPosition;
    }

    /**
     * Set LayoutManager
     *
     * @param layoutManagerType RecyclerView's layoutManager
     */
    public void setLayoutManagerType(LAYOUT_MANAGER_TYPE layoutManagerType) {
        this.layoutManagerType = layoutManagerType;
    }

}
