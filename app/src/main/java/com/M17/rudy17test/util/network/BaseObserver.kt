package com.M17.rudy17test.util.network

import android.content.Context
import android.util.Log

import androidx.annotation.CallSuper

import com.M17.rudy17test.R
import com.M17.rudy17test.api.data.gson.ApiResponse
import com.M17.rudy17test.util.Constant

import io.reactivex.Observer
import io.reactivex.exceptions.CompositeException
import retrofit2.HttpException

import com.M17.rudy17test.util.network.AppApiResponseCode.API_RESULT_ACCOUNT_BLOCK_CODE
import com.M17.rudy17test.util.network.AppApiResponseCode.API_RESULT_FORBIDDEN_CODE
import com.M17.rudy17test.util.network.AppApiResponseCode.API_RESULT_SUCCESS_CODE
import com.M17.rudy17test.util.network.AppApiResponseCode.API_RESULT_UNKNOW_ERROR
import com.M17.rudy17test.util.network.AppApiResponseCode.NETWORK_NOT_AVAILABLE

/**
 * Created by Rudy on 2019/7/21
 */
abstract class BaseObserver<T> : Observer<ApiResponse<T>> {

    private val TAG: String? = this::class.java.simpleName

    private var mContext: Context? = null

    private var errorCode = 0
    private var errorMsg: String? = ""

    /**
     * @param t
     */
    abstract fun onSuccess(t: T?, total: Int)

    /**
     * @param context
     */
    constructor(context: Context) {
        mContext = context
        errorCode = API_RESULT_UNKNOW_ERROR
        errorMsg = context.getString(R.string.alert_server_response_error)
    }

    constructor() {}

    override fun onNext(response: ApiResponse<T>) {
        if (response.isSuccess) {
            //      if(response.url.equalsIgnoreCase("http://uapi.beautybee.com/api/v2/post/latest?items=20&location=5aa62e59feff59813f8e13c4&user=5b7b9bdc33c4680f1e0bc5dc"))
            //      {
            //        blockAccountHandle();
            //      }else
            //      {
            onSuccess(response.items, response.total_count)
            //      }
        } else {
            onFailure(response.code, response.message)
        }
    }

    override fun onComplete() {}

    override fun onError(t: Throwable) {
        // 確保 正在更新 Token flag 關閉
        if (t is AppApiRetrofitClient.NoNetworkException || t is CompositeException && !checkNetWork(t)) {
            Log.d(TAG, "NoNetworkException")
            errorCode = NETWORK_NOT_AVAILABLE
            errorMsg = mContext?.getString(R.string.alert_network_error) ?: ""
        } else if (t is HttpException) {
            errorCode = t.code()
            errorMsg = t.message
        }

        onFailure(errorCode, errorMsg)
    }

    /**
     * @param code
     * @param message
     */
    @CallSuper  // if overwrite,you should let it run.
    open fun onFailure(code: Int, message: String?) {
        if (code == API_RESULT_ACCOUNT_BLOCK_CODE) {
            blockAccountHandle()
        }

        if (mContext != null) {
            if (isForbidden(code)) {
                Constant.toastAlert(mContext!!, mContext!!.getString(R.string.alert_server_response_forbidden))
            } else if (message != null && message.length > 0) {
                Constant.toastAlert(mContext!!, message)
            } else {
                Constant.toastAlert(mContext!!, mContext!!.getString(R.string.alert_server_response_error_code, code.toString()))
            }
        }
    }

    /**
     * @return True : Response Forbidden (code == 403)
     */
    private fun isForbidden(code: Int): Boolean {
        return code == API_RESULT_FORBIDDEN_CODE
    }

    private fun checkNetWork(t: Throwable): Boolean {
        val throwableList = (t as CompositeException).exceptions
        for (throwable in throwableList) {
            if (throwable is AppApiRetrofitClient.NoNetworkException)
                return false
        }
        return true
    }

    private fun blockAccountHandle() {
        //    EventBusUtil.postEvent(new AccountDisableEvent());
    }
}
