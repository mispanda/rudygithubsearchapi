package com.M17.rudy17test.util.network

import com.M17.rudy17test.api.data.gson.ApiResponse
import com.M17.rudy17test.api.data.vo.UserSearchInfoVo

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Rudy on 2019/7/21
 */
interface AppApiService {

    /**
     * 取得 使用者 任務列表
     *
     * @return Observable
     */
    @GET("/search/users")
    fun getSearchUserInfo(@Query("q") name: String, @Query("page") page: Int, @Query("per_page") perPage: Int): Observable<ApiResponse<List<UserSearchInfoVo>>>

}
