package com.M17.rudy17test.util

import com.M17.rudy17test.activity.factory.SearchViewModelFactory
import com.M17.rudy17test.api.data.repository.SearchRepository
import com.M17.rudy17test.util.network.HttpManager

import org.jetbrains.annotations.Contract

/**
 * Created by Rudy on 2019/7/21
 */
object InjectorUtils {

    /**
     * Inject HttpManager
     *
     * @return HttpManager
     */
    @Contract(" -> new")
    fun provideHttpManager(): HttpManager {
        return HttpManager()
    }

    /**
     * Inject SearchRepository
     *
     * @return UserAndProfileRepository
     */
    fun provideSearchRepository(): SearchRepository? {
        val httpManager = provideHttpManager()
        return SearchRepository.getInstance(httpManager)
    }


    /**
     * Provide Discover Image Post Detail ViewModel Factory
     *
     * @return VM Factory
     */
    fun provideSearchRepositoryVMFactory(): SearchViewModelFactory? {
        val searchRepository = provideSearchRepository()
        return searchRepository?.let { SearchViewModelFactory(it) }
    }
}
