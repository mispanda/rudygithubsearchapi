package com.M17.rudy17test.util

import org.greenrobot.eventbus.EventBus

/**
 * Created by Rudy on 2019/7/21
 */
object EventBusUtil {
    init {
        // Add EventBus index -> 可提高整個App的 EventBus工作效率
        //        EventBus.builder().addIndex(new MyEventBusIndex()).installDefaultEventBus();
    }

    fun register(subscriber: Any) {
        if (!EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().register(subscriber)
        }
    }

    fun unregister(subscriber: Any) {
        if (EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().unregister(subscriber)
        }
    }

    /**
     * 發布 normal EventBus Event
     *
     * @param event
     */
    fun postEvent(event: Any) {
        if (EventBus.getDefault().hasSubscriberForEvent(event.javaClass)) {
            EventBus.getDefault().post(event)
        }
    }

    /**
     * 發布 sticky EventBus Event
     * doc: http://greenrobot.org/eventbus/documentation/configuration/sticky-events/
     *
     * @param stickyEvent 黏黏Event
     */
    fun postStickyEvent(stickyEvent: Any) {
        EventBus.getDefault().postSticky(/* super.postSticky 具備反射邏輯 */stickyEvent)
    }

    /**
     * 主動取得 sticky EventBus Event
     * doc: http://greenrobot.org/eventbus/documentation/configuration/sticky-events/
     *
     * @param stickyEvent 黏黏Event
     * @return stickyEvent 取得與傳入 類別 對應的 黏黏Event
     */
    fun getStickyEvent(stickyEvent: Any): Any? {
        return EventBus.getDefault().getStickyEvent(stickyEvent.javaClass)
    }


    open class BaseEvent<T> {
        private var isSuccess: Boolean = false
        var errorCode: Int = 0
        var errorMsg: String? = null
        var evenData: T? = null

        fun isSuccess(): Boolean {
            return isSuccess
        }

        fun setSuccess(success: Boolean): BaseEvent<*> {
            isSuccess = success
            return this
        }
    }

    /**
     * 取得 使用者的搜尋 Event
     */
    class OnUserSearchInfoEvent<T> : BaseEvent<T>()

}

