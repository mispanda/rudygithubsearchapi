package com.M17.rudy17test.util.network

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Rudy on 2019/7/21
 */
class RxObservableUtils {

    val TAG = RxObservableUtils::class.java.simpleName
    companion object {
        /**
         * io線程執行，主線程觀察
         * .compose(RxObservableUtils.<T>applySchedulers())
        </T> */
        fun <T> applySchedulers(): ObservableTransformer<T, T> {
            return ObservableTransformer{ observable ->
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
            }
        }

        /**
         * 固定單線程 執行 用於有序需求時，主線程觀察
         * .compose(RxObservableUtils.<T>applySchedulers())
        </T> */
        fun <T> applySingleThreadSchedulers(): ObservableTransformer<T, T> {
            return ObservableTransformer{ observable ->
                observable.subscribeOn(Schedulers.single())
                        .observeOn(AndroidSchedulers.mainThread())
            }
        }
    }
}
